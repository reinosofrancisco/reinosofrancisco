| Task           | Time required | Assigned to   | Current Status | Finished | 
|----------------|---------------|---------------|----------------|-----------|
| Add Dog Pic    | > 3 minutes  |  me | Completed | &#9745;

![alt text](https://gitlab.com/reinosofrancisco/reinosofrancisco/-/raw/main/img/dog.jpg)